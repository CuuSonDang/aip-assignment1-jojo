/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uts.aip.CuuSonDang.DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import uts.aip.CuuSonDang.DTO.EntryDTO;

/**
 * This class is used for performing SQL database queries that are related to the EntryDTO class
 * @author Cuu Son Dang
 */
public class EntryDAO {
    private DataSource getDataSource() throws NamingException{
        return InitialContext.doLookup("jdbc/aip");
    }
    
    /**
     * Create a new entry
     * @param entry the entry detail that will be created
     * @return the newly created entry's ID
     * @throws NamingException
     * @throws SQLException 
     */
    public long createEntry(EntryDTO entry) throws NamingException, SQLException{
        try(Connection conn = getDataSource().getConnection();
                PreparedStatement ps = conn.prepareStatement("insert into journalEntry(journalID, entryDate, content, location) values(?,?,?,?)", new String[]{"entryID"})){
            ps.setLong(1, entry.getJournalID());
            ps.setDate(2, new Date(entry.getDate().getTime()));
            ps.setString(3, entry.getContent());
            ps.setString(4, entry.getLocation());
            
            int insertedRow = ps.executeUpdate();
            if (insertedRow != 1){
                throw new SQLException("Cannot create entry");
            }
            
            try(ResultSet rs = ps.getGeneratedKeys()){
                rs.next();
                return rs.getLong(1);
            }
        } catch (NamingException | SQLException ex){
             Logger.getLogger(EntryDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }
    
    /**
     * Retrieve an entry based on its ID
     * @param entryID the entry's unique ID
     * @return the entry detail, or null if there is not entry with the specified ID exists.
     * @throws SQLException
     * @throws NamingException 
     */
    public EntryDTO getEntry(long entryID) throws SQLException, NamingException{
        try(Connection conn = getDataSource().getConnection();
                PreparedStatement ps = conn.prepareStatement("select entryID, entryDate, content, location, journalID from journalEntry where entryID=?")){
            
            ps.setLong(1, entryID);
            
            try(ResultSet rs = ps.executeQuery()){
                if (!rs.next())
                    return null;
                
                EntryDTO entry = new EntryDTO();
                entry.setEntryID(rs.getLong(1));
                entry.setDate(rs.getDate(2));
                entry.setContent(rs.getString(3));
                entry.setLocation(rs.getString(4));
                entry.setJournalID(rs.getLong(5));
                
                return entry;
            }
            
        } catch (SQLException | NamingException ex) {
            Logger.getLogger(EntryDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }
    
    /**
     * Update an existing entry
     * @param entry the entry detail that will be updated
     * @throws NamingException
     * @throws SQLException 
     */
    public void updateEntry(EntryDTO entry) throws NamingException, SQLException{
        try(Connection conn = getDataSource().getConnection();
                PreparedStatement ps = conn.prepareStatement("update JOURNALENTRY set entryDate=?, location=?, content=? where entryID=?", new String[]{"entryID"})){
            ps.setDate(1, new Date(entry.getDate().getTime()));
            ps.setString(2, entry.getLocation());
            ps.setString(3, entry.getContent());
            ps.setLong(4, entry.getEntryID());
            
            int insertedRow = ps.executeUpdate();
            if (insertedRow != 1){
                conn.rollback();
                throw new SQLException("Cannot create entry");
            }
        } catch (NamingException | SQLException ex){
             Logger.getLogger(EntryDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }
    
    /**
     * Determine if an entry date is available for use or not
     * @param entryID the entry ID
     * @param date the date to be used for availability checking
     * @return true if the entry date is available for user, false otherwise.
     * @throws NamingException
     * @throws SQLException 
     */
    public boolean isEntryDateAvailable(long entryID, java.util.Date date) throws NamingException, SQLException{
        
        try(Connection conn = getDataSource().getConnection();
                PreparedStatement ps = conn.prepareStatement("select e1.entryID from journalentry e1, journalentry e2 "
                        + "where e1.journalID = e2.journalID and e1.entryID != e2.entryID and e1.entryID=? and e2.entryDate=?")){    
            
            ps.setLong(1, entryID);
            ps.setDate(2, new Date(date.getTime()));
            
            try (ResultSet rs = ps.executeQuery()){
                return !rs.next();
            }
            
        } catch (NamingException | SQLException ex){
             Logger.getLogger(EntryDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }
    
    /**
     * Remove an entry from the database
     * @param entryID the entry's unique ID
     * @throws NamingException
     * @throws SQLException 
     */
    public void removeEntry(long entryID) throws NamingException, SQLException{
        try(Connection conn = getDataSource().getConnection();
                PreparedStatement ps = conn.prepareStatement("delete from JOURNALENTRY where ENTRYID =?")){    
            
            ps.setLong(1, entryID);
            ps.execute();
            
        } catch (NamingException | SQLException ex){
             Logger.getLogger(EntryDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }
}
