/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uts.aip.CuuSonDang.DAO;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import uts.aip.CuuSonDang.DTO.AccountDTO;
import uts.aip.CuuSonDang.Utilities.Sha;

/**
 * This class is used for performing SQL database queries that are related to the AccountDTO class
 * @author Cuu Son Dang
 */
public class AccountDAO {
    
    private DataSource getDataSource() throws NamingException{
        return InitialContext.doLookup("jdbc/aip");
    }
    
    /**
     * Retrieve an account detail based on its username
     * @param username the unique account username
     * @return the account detail, or null if there is not account with the username exists
     * @throws NamingException
     * @throws SQLException 
     */
    public AccountDTO findAccount(String username) throws NamingException, SQLException{
        
        try(Connection conn = getDataSource().getConnection();
                PreparedStatement ps = conn.prepareStatement("select * from TRAVELER t where t.USERNAME =?")){
            ps.setString(1, username);
            
            try (ResultSet rs = ps.executeQuery()){
                boolean hasAccount = rs.next();
                
                if (!hasAccount) return null;
                
                AccountDTO account = new AccountDTO();
                account.setUsername(rs.getString("username"));
                account.setPassword(rs.getString("password"));
                account.setFullName(rs.getString("fullname"));
                account.setProfilePicture(rs.getString("profilepicture"));
                
                return account;
            }
        } catch (NamingException | SQLException ex){
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }
    
    /**
     * Check if an account with username exists in database or not
     * @param username the username to check
     * @return true if the username exists, false otherwise
     * @throws NamingException
     * @throws SQLException 
     */
    public boolean isAccountExists(String username) throws NamingException, SQLException{
        
        try(Connection conn = getDataSource().getConnection();
                PreparedStatement ps = conn.prepareStatement("select count(*) from TRAVELER t where t.USERNAME =?")){
            ps.setString(1, username);
            
            try (ResultSet rs = ps.executeQuery()){
                rs.next();
                int count = rs.getInt(1);
                
                return (count != 0);
            }
        } catch (NamingException | SQLException ex){
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }
    
    /**
     * Create a new account for traveler
     * @param accountToRegister
     * @throws NamingException if no data source named 'jdbc/aip' cannot be found
     * @throws SQLException 
     * @throws java.security.NoSuchAlgorithmException 
     */
    public void registerAccount(AccountDTO accountToRegister) throws NamingException, SQLException, NoSuchAlgorithmException{
        
        try(Connection conn = getDataSource().getConnection();
                PreparedStatement ps = conn.prepareStatement("insert into TRAVELER(USERNAME, PASSWORD, FULLNAME, PROFILEPICTURE) values(?,?,?,?)")){
            
            accountToRegister.setPassword(Sha.hash256(accountToRegister.getPassword()));
            
            ps.setString(1, accountToRegister.getUsername());
            ps.setString(2, accountToRegister.getPassword());
            ps.setString(3, accountToRegister.getFullName());
            ps.setString(4, accountToRegister.getProfilePicture());
            
            int insertedRow = ps.executeUpdate();
            if (insertedRow != 1){
                conn.rollback();
                throw new SQLException("Cannot create new account");
            }
        }
        catch (NamingException | SQLException | NoSuchAlgorithmException ex){
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }
}
