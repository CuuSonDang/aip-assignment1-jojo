/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uts.aip.CuuSonDang.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import uts.aip.CuuSonDang.DTO.AccountDTO;
import uts.aip.CuuSonDang.DTO.EntryDTO;
import uts.aip.CuuSonDang.DTO.JournalDTO;

/**
 * This class is used for performing SQL database queries that are related to the JournalDTO class
 * @author Cuu Son Dang
 */
public class JournalDAO {
    private DataSource getDataSource() throws NamingException{
        return InitialContext.doLookup("jdbc/aip");
    }
    
    /**
     * Create a new journal 
     * @param newJournal the journal's detail
     * @return the newly created journal's ID
     * @throws SQLException
     * @throws NamingException 
     */
    public long createJournal(JournalDTO newJournal) throws SQLException, NamingException{
        try(Connection conn = getDataSource().getConnection();
                PreparedStatement ps = conn.prepareStatement("insert into journal(author, title) values(?,?)", new String[]{"journalID"})){
            ps.setString(1, newJournal.getAuthor());
            ps.setString(2, newJournal.getTitle());
            
            int insertedRow = ps.executeUpdate();
            if (insertedRow != 1){
                throw new SQLException("Cannot create new journal");
            }
            
            try(ResultSet rs = ps.getGeneratedKeys()){
                rs.next();
                return rs.getLong(1);
            }
        } catch (NamingException | SQLException ex){
            Logger.getLogger(JournalDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }
    
    /**
     * Retrieve a journal based on its ID
     * @param journalID the journal's unique ID
     * @return the journal detail, or null if there is not journal with the specified ID exists
     * @throws SQLException
     * @throws NamingException 
     */
    public JournalDTO getJournal(long journalID) throws SQLException, NamingException{
        try(Connection conn = getDataSource().getConnection();
                PreparedStatement journalPS = conn.prepareStatement("select journalID, author, title from journal where journalID=?");
                PreparedStatement entryPS = conn.prepareStatement("select entryID, entryDate, content from journalEntry where journalID=? order by entryDate asc")){
            
            journalPS.setLong(1, journalID);
            entryPS.setLong(1, journalID);
            
            JournalDTO journal = new JournalDTO();
            try(ResultSet rs = journalPS.executeQuery()){
                if (!rs.next()){
                    return null;
                }
                journal.setJournalID(rs.getLong(1));
                journal.setAuthor(rs.getString(2));
                journal.setTitle(rs.getString(3));
            }
            
            try(ResultSet rs = entryPS.executeQuery()){
                while(rs.next()){
                    EntryDTO entry = new EntryDTO();
                    entry.setEntryID(rs.getLong(1));
                    entry.setDate(rs.getDate(2));
                    entry.setContent(rs.getString(3));
                    journal.getEntries().add(entry);
                }
            }
            
            return journal;
        } catch (NamingException | SQLException ex){
            Logger.getLogger(JournalDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }
    
    /**
     * Update an existing journal
     * @param journalID the journal ID that will be updated
     * @param newTitle the new title of the journal
     * @throws SQLException
     * @throws NamingException 
     */
    public void updateJournal(long journalID, String newTitle) throws SQLException, NamingException{
        try(Connection conn = getDataSource().getConnection();
                PreparedStatement ps = conn.prepareStatement("update JOURNAL set title=? where journalID=?")){
            
            ps.setString(1, newTitle);
            ps.setLong(2, journalID);
            
            int insertedRow = ps.executeUpdate();
            if (insertedRow != 1){
                conn.rollback();
                throw new SQLException("Cannot update journal");
            }
            
        } catch (SQLException | NamingException ex) {
            Logger.getLogger(JournalDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }
    
    /**
     * Get a list of journals written by an author
     * @param authorUsername the author's username
     * @return a list of journals written by the author. If the author does not have any journal, return an empty list
     * @throws SQLException
     * @throws NamingException 
     */
    public List<JournalDTO> getJournals(String authorUsername) throws SQLException, NamingException{
        List<JournalDTO> journalList = new ArrayList<>();
        
        try(Connection conn = getDataSource().getConnection();
                PreparedStatement journalPS = conn.prepareStatement("select journalID, author, title from journal where author=?");
                PreparedStatement authorPS = conn.prepareStatement("select username, fullname, profilepicture from traveler where username=?")){
            
            AccountDTO authorAccount = new AccountDTO();
            authorPS.setString(1, authorUsername);
            try(ResultSet rs = authorPS.executeQuery()){
                while (rs.next()){
                    authorAccount.setUsername(rs.getString(1));
                    authorAccount.setFullName(rs.getString(2));
                    authorAccount.setProfilePicture(rs.getString(3));
                }
            }
            
            journalPS.setString(1, authorUsername);
            try(ResultSet rs = journalPS.executeQuery()){
                while(rs.next()){
                    JournalDTO journal = new JournalDTO();
                    journal.setJournalID(rs.getLong(1));
                    journal.setAuthor(rs.getString(2));
                    journal.setTitle(rs.getString(3));
                    journal.setAuthorAccount(authorAccount);
                    journalList.add(journal);
                }
            }
            
        } catch (SQLException | NamingException ex) {
            Logger.getLogger(JournalDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
        
        return journalList;
    }
    
    /**
     * Remove a range of journals from the database
     * @param journalIDs the list that contains the IDs of the journals that will be removed
     * @throws NamingException
     * @throws SQLException 
     */
    public void deleteJournals(Long[] journalIDs) throws NamingException, SQLException{
        
        StringBuilder idBuilder = new StringBuilder();
        boolean parsedOne = false;
        for(Long journalID : journalIDs){
            if (parsedOne){
                idBuilder.append(',');
            }
            idBuilder.append(journalID);
            parsedOne = true;
        }
        
        try(Connection conn = getDataSource().getConnection();
                PreparedStatement ps = conn.prepareStatement("delete from JOURNAL where JOURNALID in (" + idBuilder.toString() + ")")){
            ps.execute();
            
        } catch (NamingException | SQLException ex){
             Logger.getLogger(JournalDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }
    
    /**
     * Get the latest updated journals
     * @return a list of the journal, along with its latest entries, ordered by entry date from the lasted to oldest
     * @throws SQLException
     * @throws NamingException 
     */
    public List<JournalDTO> getLatestJournals() throws SQLException, NamingException{
        try(Connection conn = getDataSource().getConnection();
                PreparedStatement ps = conn.prepareStatement("select j.TITLE, j.JOURNALID, e.ENTRYID, e.CONTENT, e.ENTRYDATE, e.LOCATION, t.FULLNAME, t.PROFILEPICTURE " +
                                                                    "from JOURNALENTRY e, JOURNAL j join TRAVELER t on j.AUTHOR = t.USERNAME " +
                                                                    "where e.JOURNALID = j.JOURNALID AND e.ENTRYDATE = " +
                                                                    "(select max(ENTRYDATE) from JOURNALENTRY e2 where e.JOURNALID = e2.JOURNALID) order by e.ENTRYDATE desc")){
            
            try(ResultSet rs = ps.executeQuery()){
                List<JournalDTO> lastestJournals = new ArrayList<>();
                while (rs.next()){
                    JournalDTO journal = new JournalDTO();
                    journal.setTitle(rs.getString(1));
                    journal.setJournalID(rs.getLong(2));
                    
                    EntryDTO entry = new EntryDTO();
                    entry.setEntryID(rs.getLong(3));
                    entry.setContent(rs.getString(4));
                    entry.setDate(rs.getDate(5));
                    entry.setLocation(rs.getString(6));
                    journal.getEntries().add(entry);
                    
                    AccountDTO authorAccount = new AccountDTO();
                    authorAccount.setFullName(rs.getString(7));
                    authorAccount.setProfilePicture(rs.getString(8));
                    journal.setAuthorAccount(authorAccount);
                    
                    lastestJournals.add(journal);
                }
                return lastestJournals;
            }
            
        } catch (SQLException |NamingException ex) {
            Logger.getLogger(JournalDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }
    
    /**
     * Retrieve a list of journals based on search term. The search term is case-insensitive. 
     * A journal is match with the search if its title, entry's location, or entry's content is match with the search term.
     * @param key the search term
     * @return a list of journals that match the search term. If there is not journal matched, return an empty list.
     * @throws SQLException
     * @throws NamingException 
     */
    public List<JournalDTO> search(String key) throws SQLException, NamingException{
        try(Connection conn = getDataSource().getConnection();
                PreparedStatement ps = conn.prepareStatement("select j.TITLE, j.JOURNALID, e.ENTRYID, e.CONTENT, e.ENTRYDATE, e.LOCATION, t.FULLNAME, t.PROFILEPICTURE " +
                                                            "from JOURNALENTRY e, JOURNAL j join TRAVELER t on j.AUTHOR = t.USERNAME " +
                                                            "where e.JOURNALID = j.JOURNALID " +
                                                            "    AND ( (UPPER(j.TITLE) like ? AND e.ENTRYDATE = (select max(ENTRYDATE) from JOURNALENTRY e1 where e.JOURNALID = e1.JOURNALID)) " +
                                                            "    OR e.ENTRYDATE = (select max(ENTRYDATE) from JOURNALENTRY e2 where e.JOURNALID = e2.JOURNALID AND (UPPER(e2.LOCATION) like ? OR UPPER(e2.CONTENT) like ? )) ) " +
                                                            "order by e.ENTRYDATE desc")){
            String upperKey = "%" + key.toUpperCase() + "%";
            ps.setString(1, upperKey);
            ps.setString(2, upperKey);
            ps.setString(3, upperKey);
            
            try(ResultSet rs = ps.executeQuery()){
                List<JournalDTO> lastestJournals = new ArrayList<>();
                while (rs.next()){
                    JournalDTO journal = new JournalDTO();
                    journal.setTitle(rs.getString(1));
                    journal.setJournalID(rs.getLong(2));
                    
                    EntryDTO entry = new EntryDTO();
                    entry.setEntryID(rs.getLong(3));
                    entry.setContent(rs.getString(4));
                    entry.setDate(rs.getDate(5));
                    entry.setLocation(rs.getString(6));
                    journal.getEntries().add(entry);
                    
                    AccountDTO authorAccount = new AccountDTO();
                    authorAccount.setFullName(rs.getString(7));
                    authorAccount.setProfilePicture(rs.getString(8));
                    journal.setAuthorAccount(authorAccount);
                    
                    lastestJournals.add(journal);
                }
                return lastestJournals;
            }
            
        } catch (SQLException |NamingException ex) {
            Logger.getLogger(JournalDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }
}
