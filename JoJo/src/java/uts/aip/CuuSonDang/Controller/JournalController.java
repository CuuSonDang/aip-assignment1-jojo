/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uts.aip.CuuSonDang.Controller;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import uts.aip.CuuSonDang.DAO.EntryDAO;
import uts.aip.CuuSonDang.DAO.JournalDAO;
import uts.aip.CuuSonDang.DTO.EntryDTO;
import uts.aip.CuuSonDang.DTO.JournalDTO;

/**
 * This class represents a controller that will be used to perform action related to journals as well as the entries of the journals
 * @author Cuu Son Dang
 */
@Named
@SessionScoped
public class JournalController implements Serializable {
    private JournalDTO journal = new JournalDTO();
    private List<JournalDTO> journalList = new ArrayList<>();
    private JournalDAO journalDAO = new JournalDAO();
    private boolean isModifiable = false;
    private Map<Long, Boolean> checkedJournals = new HashMap<Long, Boolean>();
    private String searchKey;
    
    private EntryDTO entry = new EntryDTO();
    private EntryDAO entryDAO = new EntryDAO();

    private static final Object syncRoot = new Object();
    
    /**
     * An entry which belong to a journal. This entry is used to load an entry for editing
     * @return an entry that can be used for editing
     */
    public EntryDTO getEntry() {
        return entry;
    }

    public void setEntry(EntryDTO entry) {
        this.entry = entry;
    }
    
    /**
     * Indicate if the current user can modify a journal as well as the entries of that journal or not
     * @return true if the user can modify the journal and its entries, false otherwise.
     */
    public boolean getIsModifiable(){
        return isModifiable;
    }
    
    /**
     * Indicate if the current user can delete a journal as well as the entries of that journal or not
     * @return true if the user can delete the journal and its entries, false otherwise.
     */
    public boolean getIsRemovable(){
        return getIsModifiable();
    }
    
    /**
     * The main journal that the user work with when edit journal, or view/add/edit entries belong to a journal
     * @return a journal with empty entry list when edit journal, or a journal with full entry list when view/add/edit entries belong to it.
     */
    public JournalDTO getJournal() {
        return journal;
    }

    public void setJournal(JournalDTO journal) {
        this.journal = journal;
    }

    /**
     * A list of journals that will be used when: user view their journals, or when they search journals using the quick search form
     * @return a list of journals that match the describe condition. Return an empty list if no journals is matched.
     */
    public List<JournalDTO> getJournalList() {
        return journalList;
    }

    public void setJournalList(List<JournalDTO> journalList) {
        this.journalList = journalList;
    }

    /**
     * A map of journal IDs and their status indicate if the user has selected the journals for delete or not
     * @return 
     */
    public Map<Long, Boolean> getCheckedJournals() {
        return checkedJournals;
    }

    public void setCheckedJournals(Map<Long, Boolean> checkedJournals) {
        this.checkedJournals = checkedJournals;
    }

    /**
     * The search key from the quick search from
     * @return 
     */
    public String getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }
    
    /**
     * Create a new journal. The author of the journal is the current logged in user.
     * @return a view that display the newly created journal
     * @throws SQLException
     * @throws NamingException 
     */
    public String createNewJournal() throws SQLException, NamingException{
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
        String author = request.getRemoteUser();
        journal.setAuthor(author);
        long journalID = journalDAO.createJournal(journal);
        return viewJournal(journalID);
    }
    
    /**
     * View a journal along with its entries
     * @param id the journal's ID
     * @return a view that display a journal along with its entries
     * @throws SQLException
     * @throws NamingException 
     */
    public String viewJournal(long id) throws SQLException, NamingException{
        try {
            journal = journalDAO.getJournal(id);
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
            String remoteUser = request.getRemoteUser();
            if (remoteUser == null) {
                isModifiable = false;
            }
            else{
                isModifiable = remoteUser.equals(journal.getAuthor());
            }
            
            return "viewJournal";
        } catch (SQLException | NamingException ex) {
            Logger.getLogger(JournalController.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }
    
    /**
     * Retrieve a list of journals created by the current logged in user
     * @return a view that display the mentioned list
     * @throws SQLException
     * @throws NamingException 
     */
    public String myJournals() throws SQLException, NamingException{
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
        String author = request.getRemoteUser();
        journalList = journalDAO.getJournals(author);
        return "myJournals";
    }
    
    /**
     * Retrieve a list of journals that contain entries from the most recent date
     * @return a list of journals that contain entries from the most recent date
     * @throws SQLException
     * @throws NamingException 
     */
    public List<JournalDTO> getLastestJournals() throws SQLException, NamingException{
        return journalDAO.getLatestJournals();
    }
    
    /**
     * Retrieve a list of journals that match the search key
     * @return a view that display the list
     * @throws SQLException
     * @throws NamingException 
     */
    public String searchJournal() throws SQLException, NamingException{
        journalList = journalDAO.search(searchKey);
        return "search";
    }
    
    /**
     * Retrieve a journal based on its ID for updating
     * @param id the journal's ID
     * @return a view that allow user to edit the entry
     * @throws SQLException
     * @throws NamingException 
     */
    public String editJournal(long id) throws SQLException, NamingException{
        journal = journalDAO.getJournal(id);
        return "editJournal";
    }
    
    /**
     * Edit the journal from user's submitted form
     * @param id the journal's ID
     * @param newTitle the journal's new title
     * @return a view that display the newly updated journal
     * @throws SQLException
     * @throws NamingException 
     */
    public String editJournal(long id, String newTitle) throws SQLException, NamingException{
        journalDAO.updateJournal(id, newTitle);
        return viewJournal(id);
    }
    
    /**
     * Delete user's journals based on the checkedJournals variable
     * @return a view display the user's remaining journals
     * @throws NamingException
     * @throws SQLException 
     */
    public String deleteSelectedJournals() throws NamingException, SQLException{
        ArrayList<Long> selectedIds = new ArrayList<>();
        for(Long journalID : checkedJournals.keySet()){
            if (checkedJournals.get(journalID) == true){
                selectedIds.add(journalID);
            }
        }
        
        journalDAO.deleteJournals(selectedIds.toArray(new Long[0]));
        return myJournals();
    }
    
    /**
     * Delete a single journal based on its ID
     * @param journalID the journal ID
     * @return a view display the user's remaining journals
     * @throws NamingException
     * @throws SQLException 
     */
    public String deleteJournal(long journalID) throws NamingException, SQLException{
        journalDAO.deleteJournals(new Long[]{journalID});
        return myJournals();
    }
    
    /**
     * Retrieve an entry for updated
     * @param entryID the entry unique ID
     * @return a view that allow user to update the entry
     * @throws SQLException
     * @throws NamingException 
     */
    public String editEntry(long entryID) throws SQLException, NamingException{
        entry = entryDAO.getEntry(entryID);
        journal = journalDAO.getJournal(entry.getJournalID());
        return "newEntry";
    }
    
    /**
     * Display a view for the user to create new entry
     * @param journalID the ID of the journal that the entry belongs to
     * @return a view that allow user to create new entry
     * @throws SQLException
     * @throws NamingException 
     */
    public String newEntry(long journalID) throws SQLException, NamingException{
        entry = new EntryDTO();
        journal = journalDAO.getJournal(journalID);
        return "newEntry";
    }
    
    /**
     * Insert an entry if it has no ID, or update an existing entry based on its unique ID
     * @param journalID the ID if the journal that the entry belongs to
     * @return a view that display the journal with newly created/updated entry
     * @throws NamingException
     * @throws SQLException 
     */
    public String saveEntry(long journalID) throws NamingException, SQLException{
        synchronized(syncRoot){
            entry.setJournalID(journalID);
            if (entryDAO.getEntry(entry.getEntryID()) == null){
                entryDAO.createEntry(entry);
            } else {
                entryDAO.updateEntry(entry);
            }
            
            return viewJournal(journalID);
        }
    }
    
    /**
     * Remove an entry
     * @param entryID the entry unique ID
     * @return a view that display the journal with the remaining entries
     * @throws NamingException
     * @throws SQLException 
     */
    public String deleteEntry(long entryID) throws NamingException, SQLException{
        entryDAO.removeEntry(entryID);
        return viewJournal(journal.getJournalID());
    }
}
