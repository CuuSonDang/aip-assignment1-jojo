/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uts.aip.CuuSonDang.Controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.naming.NamingException;
import javax.servlet.http.Part;
import uts.aip.CuuSonDang.DAO.AccountDAO;
import uts.aip.CuuSonDang.DTO.AccountDTO;
import uts.aip.CuuSonDang.Utilities.FileUltility;

/**
 * This class contains action that allow the user to register new account
 * @author Cuu Son Dang
 */
@Named
@RequestScoped
public class RegisterController {

    private AccountDTO account = new AccountDTO();
    private AccountDAO accountDAO = new AccountDAO();
    private String errorMessage = null;
    private static final Object syncRoot = new Object();
    private Part profilePicture;

    /**
     * The user's uploaded image that will be used for his/her account profile picture
     * @return 
     */
    public Part getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(Part profilePicture) {
        this.profilePicture = profilePicture;
    }

    /**
     * The account that will be used for registration
     * @return 
     */
    public AccountDTO getAccount() {
        return account;
    }

    public void setAccount(AccountDTO account) {
        this.account = account;
    }
    
    /**
     * 
     * @return 
     */
    public String getErrorMessage(){
        return this.errorMessage;
    }

    public String register() {
        try{
           synchronized (syncRoot) {
                if (profilePicture != null){
                    try (InputStream input = profilePicture.getInputStream()){
                        // upload profile picture
                        String imgUploadFolder = FacesContext.getCurrentInstance().getExternalContext().getRealPath("resources/imgs");
                        String relativePicturePath = "userProfilePictures" + File.separator + account.getUsername() + "." + FileUltility.getExtension(profilePicture.getSubmittedFileName());
                        String profilePicturePath = imgUploadFolder + File.separator + relativePicturePath;
                        FileUltility.copy(input, profilePicturePath);
                        // set uploaded link to accountDTO
                        account.setProfilePicture(relativePicturePath);
                    } 
                }

                accountDAO.registerAccount(account);
                return "welcome";
            } 
        } catch (IOException | NamingException |SQLException | NoSuchAlgorithmException ex){
            Logger.getLogger(RegisterController.class.getName()).log(Level.SEVERE, null, ex);
            errorMessage = "There was an error during account registration, please try again";
            return "";
        }
        
    }
}
