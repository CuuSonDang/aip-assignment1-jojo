/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uts.aip.CuuSonDang.Controller;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import uts.aip.CuuSonDang.DAO.AccountDAO;

/**
 * This class allow the user to login/logout of the system
 * @author Cuu Son Dang
 */
@Named
@RequestScoped
public class LoginController implements Serializable {
    private AccountDAO accountDAO = new AccountDAO();
    
    private String username;
    private String password;
    private String errorMessage;
    
    /**
     * The unique username of the user
     * @return 
     */
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * The user's password
     * @return 
     */
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    /**
     * A user friendly message to be displayed to user
     * @return 
     */
    public String getErrorMessage() {
        return errorMessage;
    }
    
    /**
     * Login a user based on the username and password
     * @return the index view if the user login successfully, or return to login page if there is error during login.
     */
    public String login() {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
            request.login(username, password);
            return "index";
        } catch (ServletException ex ) {
            errorMessage = "Login failed, please try again";
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
            return "login";
        }
    }
    
    /**
     * Log a user out of the system
     * @return the index view if user log out successfully
     * @throws javax.servlet.ServletException
     */
    public String logout() throws ServletException{
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
            request.logout();
            return "index";
        } catch (ServletException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }
}
