/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uts.aip.CuuSonDang.Controller;

import java.io.Serializable;
import java.sql.SQLException;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import uts.aip.CuuSonDang.DAO.AccountDAO;
import uts.aip.CuuSonDang.DTO.AccountDTO;

/**
 * This class is used to retrieve the information of the logged in user
 * @author Cuu Son Dang
 */
@Named
@SessionScoped
public class UserController implements Serializable {
    private AccountDTO loggedInUser = null;
    private AccountDAO accountDAO = new AccountDAO();
    
    /**
     * Indicate if the user is logged in or not
     * @return true if the user is logged in, false otherwise.
     */
    public boolean getIsLoggedIn(){
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
        return request.getRemoteUser() != null;
    }
    
    /**
     * Retrieve the information of the logged in user
     * @return the account detail of the logged in user
     * @throws NamingException
     * @throws SQLException 
     */
    public AccountDTO getLoggedInUser() throws NamingException, SQLException {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
        if (loggedInUser == null || !loggedInUser.getUsername().equals(request.getRemoteUser())){
            loggedInUser = accountDAO.findAccount(request.getRemoteUser());
        }
        
        return this.loggedInUser;
    }
}
