/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uts.aip.CuuSonDang.Validator;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;
import javax.naming.NamingException;
import uts.aip.CuuSonDang.DAO.AccountDAO;

/**
 * A custom validator to make sure the selected username is unique
 * @author Cuu Son Dang
 */
@Named
@FacesValidator
public class UniqueUsernameValidator implements Validator {

    private AccountDAO accountDAO = new AccountDAO();

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        try {
            String usernameToValidate = (String) value;
            boolean usernameExists = accountDAO.isAccountExists(usernameToValidate);
            if (usernameExists) {
                String errorMsg = String.format("Username %s is already exists, please choose another.", usernameToValidate);
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMsg, null));
            }
        } catch (NamingException | SQLException ex) {
            Logger.getLogger(UniqueUsernameValidator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
