/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uts.aip.CuuSonDang.Validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;
import javax.servlet.http.Part;

/**
 * A custom validator to ensure the upload file is an image
 * @author Cuu Son Dang
 */
@Named
@FacesValidator
public class ImageUploadValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        Part picture = (Part) value;
        if (picture != null) {
            if (!picture.getContentType().contains("image")) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Please select a image file type (jpg, png, etc.)", null));
            }
        }
    }

}
