/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uts.aip.CuuSonDang.Validator;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;
import javax.naming.NamingException;
import uts.aip.CuuSonDang.DAO.EntryDAO;

/**
 * A custom validator to ensure the entry date is valid
 * @author Cuu Son Dang
 */
@Named
@FacesValidator
public class EntryDateValidator implements Validator {

    private EntryDAO entryDAO = new EntryDAO();
    
    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        try {
            Date entryDate = (Date)value;
            long entryID = (long) component.getAttributes().get("entryID");
            
            if (!entryDAO.isEntryDateAvailable(entryID, entryDate)){
                String errorMsg = String.format("There is already an entry for the date %s", new SimpleDateFormat("dd/MM/yyyy").format(entryDate));
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMsg, null));
            }
        } catch (NamingException | SQLException ex) {
            Logger.getLogger(EntryDateValidator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
