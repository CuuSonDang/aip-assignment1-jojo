/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uts.aip.CuuSonDang.Validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;

/**
 * A custom validator to ensure the user's password and confirm password is correct
 * @author Cuu Son Dang
 */
@Named
@FacesValidator
public class ConfirmPasswordValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String confirmPassword = (String) value;
        UIInput passwordInput = (UIInput) component.getAttributes().get("password");
        String selectedPassword = (String) passwordInput.getValue();

        if (selectedPassword == null || confirmPassword == null) {
            return;
        }

        if (!confirmPassword.equals(selectedPassword)) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Password and confirm password do not match", null));
        }
    }

}
