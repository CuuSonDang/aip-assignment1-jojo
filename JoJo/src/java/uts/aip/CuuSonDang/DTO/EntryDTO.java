package uts.aip.CuuSonDang.DTO;

import java.util.Date;
import javax.validation.constraints.Size;

/**
 * This class represents a entry, written by an author about a specific location that his/her has visited on a specific date, which belongs to a specific journal
 * @author Cuu Son Dang
 */
public class EntryDTO {
    private long entryID;
    private long journalID;
    private Date date;
    @Size(min=1, message = "Please enter entry's content")
    private String content;
    private String location;
    
    /**
     * Get the entry's unique ID
     * @return the entry's ID
     */
    public long getEntryID() {
        return entryID;
    }

    public void setEntryID(long entryID) {
        this.entryID = entryID;
    }

    /**
     * Get the date that the entry was written about
     * @return the entry's date
     */
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Get the content of the entry
     * @return the entry content
     */
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    /**
     * Get the location that the entry is written about
     * @return the entry's location
     */
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * Get the journal's unique ID that the entry belongs to
     * @return the journal's ID
     */
    public long getJournalID() {
        return journalID;
    }

    public void setJournalID(long journalID) {
        this.journalID = journalID;
    }
}
