package uts.aip.CuuSonDang.DTO;

import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * This class represents a travel diary, written by an author about his/her trip
 */
public class JournalDTO {
    
    @NotNull
    private String author;
    @NotNull
    @Size(min=6, max=255, message = "Title's length should be between 6-255 chracters (inclusive)")
    private String title;
    @NotNull
    private long journalID;
    private List<EntryDTO> entries = new ArrayList<>();
    private AccountDTO authorAccount;
    
     /**
     * Create a new journal
     */
    public JournalDTO(){
    }
    
    /**
     * Retrieve a list of entries in the journal
     * @return a list of entries, if the journal has no entry, return an empty list
     */
    public List<EntryDTO> getEntries() {
        return entries;
    }
    
    /**
     * Get the unique ID of the journal
     * @return the journal's unique ID
     */
    public long getJournalID() {
        return journalID;
    }

    /**
     * Set the ID for the journal
     * @param journalID the unique ID of the journal
     */
    public void setJournalID(long journalID) {
        this.journalID = journalID;
    }
    
    /**
     * Get the username of the journal's author
     * @return author's username
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Set the username of the journal's author
     * @param author the username of the author, must not null
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * Get the title of the journal
     * @return the title of the journal
     */
    public String getTitle() {
        return title;
    }

    /**
     * Set the title of the journal.
     * @param title the title of the journal, must not null and has a length between [6-255] characters (inclusive)
     */
    public void setTitle(String title) {
        this.title = title;
    }
    
    /**
     * Get the account of the author
     * @return author's account
     */
    public AccountDTO getAuthorAccount() {
        return authorAccount;
    }

    /**
     * Set the author's account
     * @param authorAccount author's account
     */
    public void setAuthorAccount(AccountDTO authorAccount) {
        this.authorAccount = authorAccount;
    }
}
