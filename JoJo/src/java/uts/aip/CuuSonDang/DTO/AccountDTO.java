package uts.aip.CuuSonDang.DTO;

import javax.validation.constraints.Size;

/**
 * This class represents an user's account detail
 * @author Cuu Son Dang
 */
public class AccountDTO {
    @Size(min = 6, max = 32, message = "Username must be between 6-32 characters long")
    private String username;
    @Size(min = 8, message = "Password must be at least 8 characters long")
    private String password;
    @Size(max = 255, message = "Your full name is too long, it must not exceed 255 characters")
    private String fullName;
    private String profilePicture;

    /**
     * A path to the account profile picture
     * @return a path to the profile picture, or null if the account does not have a profile picture
     */
    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }
    
    /**
     * The unique username of the account
     * @return the accounts username
     */
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 
     * @return the account password
     */
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 
     * @return the account owner's full name
     */
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
