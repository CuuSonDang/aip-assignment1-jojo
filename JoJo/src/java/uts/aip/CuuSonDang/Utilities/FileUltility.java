/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uts.aip.CuuSonDang.Utilities;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Helper class for file-related action
 * @author Cuu Son Dang
 */
public class FileUltility {
    /**
     * Copy a file input stream to a location
     * @param input the file input stream
     * @param path the location where the file will be copied to
     * @throws IOException 
     */
    public static void copy(InputStream input, String path) throws IOException{
        try(OutputStream output = new FileOutputStream(path)){
            byte[] buffer = new byte[1024];
            int len;
            while((len=input.read(buffer))>0){
                output.write(buffer,0,len);
            }
        } catch (IOException ex) {
            Logger.getLogger(FileUltility.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }
    
    /**
     * Get the extension of a file, based on its path
     * @param path the path to the file, it can be either relative path or absolute path
     * @return 
     */
    public static String getExtension(String path){
        String extension = "";

        int i = path.lastIndexOf('.');
        if (i > 0) {
            extension = path.substring(i+1);
        }
        
        return extension;
    }
}
