jQuery.expr[':'].regex = function(elem, index, match) {
    var matchParams = match[3].split(','),
        validLabels = /^(data|css):/,
        attr = {
            method: matchParams[0].match(validLabels) ? 
                        matchParams[0].split(':')[0] : 'attr',
            property: matchParams.shift().replace(validLabels,'')
        },
        regexFlags = 'ig',
        regex = new RegExp(matchParams.join('').replace(/^\s+|\s+$/g,''), regexFlags);
    return regex.test(jQuery(elem)[attr.method](attr.property));
};

function readImg(input, previewClass) {

    if (input.files && input.files[0]) {
        
//        var file = input.files[0];
//        if (!file.type.match('image.*')) {
//            $(input).val("");
//            alert("Please select an image file type");
//            return;
//        }
        
        var reader = new FileReader();

        reader.onload = function (e) {
            $(previewClass).attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

$(document).on("change", "input:file:regex(class, img-input-[0-9]+)", function(){
    var imgClass = $(this).attr("class").match("img-input-[0-9]+")[0];
    var number = imgClass.substr(10);
    var previewClass = ".img-preview-" + number;
    readImg(this, previewClass);
});
