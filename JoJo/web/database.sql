drop view jdbcrealm_traveler;
drop view jdbcrealm_group;
drop table journalEntry;
drop table journal;
drop table traveler;


create table traveler(
    username varchar(255) not null primary key,
    password varchar(255) not null,
    fullname varchar(255) not null,
    profilepicture varchar(255)
);

create view jdbcrealm_traveler (username, password) as
select username, password
from traveler;

create view jdbcrealm_group (username, groupname) as
select username, 'Users'
from traveler;

create table journal(
    journalID bigint not null primary key generated always as identity (start with 1, increment by 1),
    author varchar(255) not null references traveler(username),
    title varchar(255) not null
);

create table journalEntry(
    entryID bigint not null primary key generated always as identity (start with 1, increment by 1),
    journalID bigint not null references journal(journalID) on delete cascade,
    entryDate date not null,
    location varchar(255) not null,
    content clob
);

insert into traveler(username, password, fullname) values('sondang', 'd2d646aa1c28149e3d412a3204b776f12681fe14f5619fdbdee877942d29ab4a', 'Cuu Son Dang');
insert into journal(author, title) values('sondang', 'Summer in Europe');
insert into journalEntry(journalID, entryDate, content, location) values(1, '08/24/2016', 'SiE - Entry 1', 'Paris');
insert into journalEntry(journalID, entryDate, content, location) values(1, '08/28/2016', 'SiE - Entry 2', 'Spain');

insert into journal(author, title) values('sondang', 'Australia Winter');
insert into journalEntry(journalID, entryDate, content, location) values(2, '08/22/2016', 'AW - Entry 1', 'Blue Mountains');
insert into journalEntry(journalID, entryDate, content, location) values(2, '08/30/2016', 'AW - Entry 2', 'Sydney');

select e.ENTRYID, e.CONTENT, e.ENTRYDATE, j.TITLE, t.FULLNAME, t.PROFILEPICTURE
from JOURNALENTRY e, JOURNAL j join TRAVELER t on j.AUTHOR = t.USERNAME
where 
    e.JOURNALID = j.JOURNALID
    AND e.ENTRYDATE = (select max(ENTRYDATE) from JOURNALENTRY e2 where e.JOURNALID = e2.JOURNALID)
order by e.ENTRYDATE desc;

select e.ENTRYID, e.CONTENT, e.ENTRYDATE, j.TITLE, t.FULLNAME, t.PROFILEPICTURE
from JOURNALENTRY e, JOURNAL j join TRAVELER t on j.AUTHOR = t.USERNAME
where 
    e.JOURNALID = j.JOURNALID
    AND ( (UPPER(j.TITLE) like '%SUMMER%' AND e.ENTRYDATE = (select max(ENTRYDATE) from JOURNALENTRY e1 where e.JOURNALID = e1.JOURNALID))
    OR e.ENTRYDATE = (select max(ENTRYDATE) from JOURNALENTRY e2 where e.JOURNALID = e2.JOURNALID AND (UPPER(e2.LOCATION) like '%SUMMER%' OR UPPER(e2.CONTENT) like '%SUMMER%' )) )
order by e.ENTRYDATE desc;